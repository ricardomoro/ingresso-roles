<?php
/**
 * Plugin Name: Ingresso Roles
 * Plugin Description: Roles para o Portal de Ingresso do IFRS.
 */
 
add_action('init', function() {
    // Campus Role
    if (!get_role( 'campus' )) {
        add_role('campus', __('Usuário de Campus'), array(
            'read'                     => true,
            'upload_files'             => true,

            'publish_resultados'       => true,
            'edit_resultados'          => true,
            'edit_others_resultados'   => false,
            'delete_resultados'        => true,
            'delete_others_resultados' => false,
            'read_private_resultados'  => true,
            'edit_resultado'           => true,
            'delete_resultado'         => true,
            'read_resultado'           => true,

            'assign_campi'             => true,
            'assign_modalidade'        => true,
        ));
    }
    // Ensino Role
    if (!get_role( 'ensino' )) {
        add_role('ensino', __('Usuário do Ensino'), array(
            'read'                 => true,
            'upload_files'         => true,

            'publish_cursos'       => true,
            'edit_cursos'          => true,
            'edit_others_cursos'   => false,
            'delete_cursos'        => true,
            'delete_others_cursos' => false,
            'read_private_cursos'  => true,
            'edit_curso'           => true,
            'delete_curso'         => true,
            'read_curso'           => true,

            'assign_campi'         => true,
            'assign_modalidade'    => true,
            'assign_turno'         => true,
        ));
    }
});
